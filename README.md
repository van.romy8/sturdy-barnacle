# Chebyshev distance

The Chebyshev distance, also known as the maximum metric is a metric defined on a vector space where the distance between two vectors is the greatest of their differences along any coordinate dimension. 


## Example 
Consider two points p=(1,2,3) and q=(4,0,6) in a 3-dimensional space. The **Chebyshev distance** between them is calculated as follows:

```python
1. Calculate the absolute differences along each dimension:

∣1−4∣=3
∣2−0∣=2
∣3−6∣=3


2.Take the maximum of these differences:

max(3,2,3)=3
So, the Chebyshev distance between p and q is 3.
```